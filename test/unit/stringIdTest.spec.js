const users = require('../../src/user');

test('Exception', () => {
    expect(() => {
        users("hello world")
    }).toThrowError("id needs to be integer");
});
