var config = require('../../nightwatch.conf.js');

module.exports = {
    before : function(browser) {
        browser.resizeWindow(1200, 1200)
    },

    'tpt tunniplaan': function(browser) {
        browser
        .url('http://www.tptlive.ee')
        .waitForElementVisible('body', 1000)
        .saveScreenshot(config.imgpath(browser) + 'tptliveEsileht.png')
        .click('li[id=menu-item-1313]')
        .pause(2000)
        .waitForElementVisible('body', 1000)
        .saveScreenshot(config.imgpath(browser) + 'tunniplaanMenu.png')
        .waitForElementVisible('a.plan_button_class_span_groups.btn-default.btn.btn-lg')
        .click('a[href="https://tpt.siseveeb.ee/veebivormid/tunniplaan/tunniplaan?oppegrupp=226&nadal=03.12.2018"]')
        .saveScreenshot(config.imgpath(browser) + 'tunniplaanKlass.png')
        .pause(2000)
        .end();
    }
};