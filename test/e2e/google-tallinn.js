var config = require('../../nightwatch.conf.js');

module.exports = {
    'google tallinn': function(browser) {
        browser
        .url('http://www.google.ee')
        .waitForElementVisible('body', 1000)
        .assert.title('Google')
        .assert.visible('input[type=text]')
        .setValue('input[type=text]', 'tallinn')
        .waitForElementVisible('input[name=btnK]', 1000)
        .click('input[name=btnK]')
        .pause(1000)
        .assert.containsText('div#rhscol.col', 'Tallinn')
          
        .click('h3.LC20lb:nth-of-type(1)')
        .pause(1000)
        .saveScreenshot(config.imgpath(browser) + 'firstresult.png')
        .end();
    }
};