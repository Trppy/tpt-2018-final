module.exports = function capitalize(stringy) {
    if (typeof stringy !== 'string') {
      throw new Error('bad input');
    }
    return stringy[0].toUpperCase() + stringy.slice(1);
  };
